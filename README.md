test-meta-jailhouse

Repo för att lätt sätta upp testning av meta-jailhouse. Använd make för att
bygga targets, tex

    make jailhouse-image

För att köra bitbake manuellt, tex

    make yocto-bash
    bitbake jailhouse-image

