DOCKER_BUILD_IMAGE=codiax/yocto-build

# BUILD_ID is a unique identifier for this specific build directory
# It is used as a version tag for docker images, and tag for the build volume
# It is needed to support many distinct build environments using the same base
# image name in the same build host
BUILD_ID = $(shell pwd | md5sum | cut -c 1-4)

# UID - User id is used for the creation of the docker image
UID = $(shell id -u ${USER})

# SUBMODULES lists all git submodules that are used in the build
# SUBMODULE_IDS lists all corresponding .git directories. Their existence is
# used to identify if the submodule has been initialized or not
#
SUBMODULES := poky meta-jailhouse meta-openembedded meta-sunxi
SUBMODULE_IDS := $(foreach module,$(SUBMODULES),layers/$(module)/.git)

# start docker container with bash terminal setup for bitbake builds
#
.PHONY: yocto-bash
yocto-bash: dbuild .temp-yocto-bash-init
	@docker run --tty --interactive --rm \
		--user "user:user" \
		--workdir ${PWD} \
		--volume ${PWD}:${PWD}:rw \
		$(DOCKER_BUILD_IMAGE):$(BUILD_ID) \
		bash --init-file .temp-yocto-bash-init

# Build a target image (any *-image) using Yocto/OpenEmbedded
##
.PHONY: %-image
%-image: dbuild
	@echo "*** Building target image $@"
	$(Q)docker run --tty --interactive --rm \
		--user "user:user" \
		--workdir ${PWD} \
		--volume ${PWD}:${PWD}:rw \
		$(DOCKER_BUILD_IMAGE):$(BUILD_ID) \
		bash -c ". layers/poky/oe-init-build-env dbuild && bitbake $*-image"

# create dbuild dir with build environment (docker build environment)
#
dbuild: $(SUBMODULE_IDS) | $(DOCKER_BUILD_IMAGE)
	@docker run --tty --interactive --rm \
		--user "user:user" \
		--workdir ${PWD} \
		--volume ${PWD}:${PWD}:rw \
		$(DOCKER_BUILD_IMAGE):$(BUILD_ID) \
		bash -c "TEMPLATECONF=../conf . layers/poky/oe-init-build-env dbuild"

# build docker image used for yocto based builds
#
.PHONY: $(DOCKER_BUILD_IMAGE)
$(DOCKER_BUILD_IMAGE):
	@if ! docker inspect $(DOCKER_BUILD_IMAGE):$(BUILD_ID) 2>&1 >/dev/null; then \
		docker build -t $(DOCKER_BUILD_IMAGE):$(BUILD_ID) \
		 --build-arg USERID=${UID} \
		 --file docker/Dockerfile docker; \
	fi;

# create support file for bash environment initialization of yocto build
#
.temp-yocto-bash-init:
	@echo ". layers/poky/oe-init-build-env dbuild" > .temp-yocto-bash-init

# Update the git submodule and populate it's directory
#
layers/%/.git:
	$(Q)git submodule update --init --recursive layers/$*

# clean targets
#
.PHONY: clean clean-docker clean-images clean-all
clean:
	rm -rf dbuild/cache dbuild/tmp

clean-docker:
	@docker rmi $(DOCKER_BUILD_IMAGE):$(BUILD_ID)

distclean cleanall: clean-docker
	rm -rf dbuild
	rm -f .temp-yocto-bash-init



# print variable value
# Use on the command line:   make print-VARNAME
#
print-%: ; @echo $*=$($*)
