#!/bin/bash

TOPDIR=$(pwd)

if [ ! -d ./poky ]; then
    wget -q https://git.yoctoproject.org/cgit/cgit.cgi/poky/snapshot/morty-16.0.2.tar.gz
    tar xf morty-16.0.2.tar.gz
    mv morty-16.0.2 poky
    cd -
fi


mkdir -p layers
cd layers

if [ ! -d ./meta-openembedded ]; then 
    git clone git://git.openembedded.org/meta-openembedded
    cd meta-openembedded
    git checkout morty
    cd -
fi


if [ ! -d ./meta-sunxi ]; then 
    git clone https://github.com/linux-sunxi/meta-sunxi
    cd meta-sunxi
    git checkout morty
    cd -
fi


if [ ! -d ./meta-jailhouse ]; then 
    git clone ${MJH_REPO} meta-jailhouse
fi

cd meta-jailhouse
git pull
git checkout ${MJH_BRANCH}

cd "$TOPDIR"

source poky/oe-init-build-env
bitbake-layers add-layer ../layers/meta-jailhouse
bitbake-layers add-layer ../layers/meta-sunxi
bitbake-layers add-layer ../layers/meta-openembedded/meta-oe
bitbake-layers add-layer ../layers/meta-openembedded/meta-python

echo "CELLS_pn-jailhouse = \"freertos-cell\"" >> conf/local.conf
